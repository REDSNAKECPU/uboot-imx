#!/bin/sh

export ARCH=arm 
export CROSS_COMPILE=arm-poky-linux-gnueabi-

make distclean

make mx6ul_14x14_evk_defconfig

make -j4 
